---
title: "Onepage Storytelling Website"
date: 2022-08-05T15:40:24+06:00
image : "images/Onepage Wagenfeld Webseite/wagenfeld-webseite5.0.png"
# author
author : ["Diyar"]
# meta descriptionauthor : ["Diyar"]
# categories
categories: ["Web dev"]
tags: ["Webseite", "Storytelling", "HTML,CSS,JS"]
description: "This is meta description"
# save as draft
draft: false
---

<h4>Implementation of a One Page Storytelling Website with GSAP
 Animations und Sound effects<br />HTML CSS JS</h4>

<p style="font-size: 120%">
As part of the international course in media informatics at the Bremen University of Applied Sciences
a Bachelor Project for the exhibition "Wilhelm Wagenfeld A to Z" by Wilhelm Wagenfeldhaus
planned in Bremen. This project is intended to be the digital part of the previous analogue exhibition
deliver. The aim of the "Wagenfelds Storytelling" project is to create a one-page website that
brings visitors closer to the life of Wilhelm Wagenfeld and his acquaintances.
The target group for this project are people of all ages who are interested in history and design.
I have developed the website based on the prototypes that I
created with Adobe XD in the previous semester. Thereby
I had the opportunity to present my first design examples on the
Browser to test and the design with many new ideas.
</p>
<br>
<br>

 <!--<img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite1.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
<br>-->
 <img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite2.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
 <p style="font-size:150%">The opening page lights up using opacity when user drags down. </p>
<br>
 <img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite3.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
<p style="font-size:150%">Cinematic video of the old town. </p>
<br>
 <img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite4.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
<p style="font-size:150%">Where the story begins, the text scrolls dynamically from the left to the right of the screen with the help of a gap animation(js).</p>
<br>
 <img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite5.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
<p style="font-size:150%">As the user scrolls down, the images move in the opposite direction and the texts appear after a few seconds. </p>
<br>
 <img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite6.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
<p style="font-size:150%">With the help of pop up, extra content can be opened without disturbing the flow of the main story.
</p>
<br>
 <img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite7.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
 <p style="font-size:150%">Additional content with images.
</p>
<br>
<img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite10.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
<p style="font-size:150%">The background images combined with the help of photoshop offer an effective visual with the text.</p>
<br>
<img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite13.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
<br>
 <img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite14.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />
<br>
<img  src="/images/Onepage Wagenfeld Webseite/wagenfeld-webseite15.png" alt="Wilhelm Wagefeld Webseite Secreenshot" />