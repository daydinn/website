---
title: "Prototyp of a Stroytelling Website "
date: 2021-06-14T15:40:24+06:00
image : "images/Wagenfeld Prototyp/prototyp9.0.png"
# author
author : ["Diyar"]
# categories
categories: ["Web Design"]
tags: ["Prototyp", "Webseite", "Adobe CC"]
# meta description
description: "This is meta description"
# save as draft
draft: false
---

<h3>Prototyp of One Page Storytelling Website<br />made with Adobe XD </h3>

<h4>Project ideas</h4>
<p style="font-size:120%">
Implementation of a one-page website with storytelling
elements<br>
Biography of Wilhelm Wagenfeld<br>
Presentation of his works<br>
Perspectives and conditions of the time<br>
Wagenfeld's impact on the present<br>
Some other contemporaries and their style<br>
</p>
<h4>Project goals</h4>
<p style="font-size:120%">
Digital representation of the website with storytelling
elements<br>
Aesthetic processing with animations and
sound effects<br>
Simple design and diverse informations<br>
Chronological order of the content<br>
Highlighting of individual works of art<br>
Insight 20th century
</p>
<h4>Target Group</h4>
<p style="font-size:120%">
People who are interested in Wilhelm
Wagenfeld and his designs<br>
Nostalgia, history and art fans<br>
Internet Community (Reddit, Twitter
e.g.), artists, designers
</p>
<h4>Grid</h4>
<p style="font-size:120%">To determine the grid, I have tested several variants and
found the standard 12-column grid to be the most appropriate.
Bootstrap comes with a 12-column grid layout, which is 940 pixels wide.
It contains predefined classes for layout options, also for generating more
powerful semantic layouts mixed classes.</p>
<h4>Conclution</h4>
<p style="font-size:120%">
I tried to create the content based on best practice examples that I
have impressed. One of my goals was with lots of background images,
animations and sound effects to make users feel as if they were in a journey through time
without imposing a specific content or point of view. With
the diversity and dynamics of the content and optional additional popups the website should arouse curiosity,
which encourages users to explore the site freely.
</p>


<img  src="/images/Wagenfeld Prototyp/prototyp2.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">Where the story begins.</p>
<br>
<img  src="/images/Wagenfeld Prototyp/prototyp3.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">His education in Bauhaus.</p>
<br>
<img  src="/images/Wagenfeld Prototyp/prototyp4.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">Some of the contemporaries and their life.</p>
<br>
<img  src="/images/Wagenfeld Prototyp/prototyp5.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">The story of the discovery of the famous table lamp.</p>
<br>
<img  src="/images/Wagenfeld Prototyp/prototyp6.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">The story of the discovery of the famous table lamp.-2</p>

<br>
<img  src="/images/Wagenfeld Prototyp/prototyp7.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">The story of the discovery of the famous table lamp.-3</p>
<br>
<img  src="/images/Wagenfeld Prototyp/prototyp8.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">The table lamp.</p>

<br>
<img  src="/images/Wagenfeld Prototyp/prototyp9.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">The bauhaus director of the period and his quote.</p>
<br>
<img  src="/images/Wagenfeld Prototyp/prototyp10.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">Bauhaus closure due to political pressure and some information.</p>
<br>
<img  src="/images/Wagenfeld Prototyp/prototyp11.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">Teachers who had to seek asylum abroad.</p>
<br>
<img  src="/images/Wagenfeld Prototyp/prototyp12.png" alt="Wilhelm Wagefeld Prototyp Secreenshot" />
<p style="font-size:150%">Wagenfeld after-school career and advertising campaigns.</p>

<br>