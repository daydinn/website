---
title: "Prototyp of a Digital Impact Lab"
date: 2022-10-14T15:40:24+06:00
image : "images/Toolbuilding2/toolbuilding-pro17.1.png"

# author
author : ["Diyar"]
# categories
categories: ["Prototyps"]
tags: ["Prototyp", "Digital IMpact Lab", "Adobe CC"]
# meta description
description: "This is meta description"
# save as draft
draft: false
---

<h3>Prototyp of a digital impact lab <br />Adobe XD & Illustrator</h3>
<br>

<h4>Motivation </h4>

<p style="text-align: left; font-size:150%">
Germany is in an ongoing digital transformation. Digital Impact Labs give citizens the opportunity to learn more about technologies, regardless of whether they have previous technical knowledge or not. In Digital Impact Labs, citizens of all ages can try out 3D printers or learn how to use Office programs. The Digital Impact Labs are initiated and largely funded by the communities. In order to better accompany the digital change in Germany, more digital impact labs are needed.
Therefore, the project motivation consisted of the goal of facilitating the creation of Digital Impact Labs. By helping the communities present the important information that is needed and sometimes also requires a high level of knowledge about technologies in a clear and understandable way. Above all, the information that is important for the community should be presented, and this information should be presented in a particularly understandable way. Not too much information should appear at once, and helpful graphics should be used.
</p>
<h4>Concept</h4>
<p style="text-align: left; font-size:150%">
The consultants play an important role in operating the Digital Impactlab.
They are responsible for going to the church to gather information about it to find out what else may be needed.
Then this information is added to the rooms and resources so that the tool weighs the available resources and ranks the needed resources by importance.
To make it more effective for new users, the app also provides some easy-to-understand information about technologies that can be used in a digital impact lab.
Also, the app generates certain pages showing the current progress that can be shared with the community.
This will update the content to allow new information to be entered later.
</p>

<h4>Visualization</h4>
<p style="text-align: left; font-size:150%">
The app should be designed to be barrier-free and responsive and made accessible from all platforms in order to appeal to more people. Organizational and communication information should be easily accessible and visualized in a simple way.
A mind map shows what is available and who can provide it. This creates a network of people and objects while presenting the information in an easy-to-understand way. The various resources should be sorted according to their importance and given a corresponding number of points.
The aim is to “sell” a Digital Impact Lab to the municipality. Therefore, the tool needs to highlight the benefits and feasibility of a lab for the community and the people.
In the first steps, I thought about what kind of information is important for the municipalities and how I can prepare it visually. On the one hand, it is important that the connections become clear in order to show what is already there and who is involved. On the one hand, an overview of the existing network should be created, on the other hand, it should also highlight who is particularly involved. In order to also present possible more complex structures, I have not opted for a list view, but  a mind map.
</p>
<h4>Conclusion</h4>
<p style="text-align: left; font-size:150%">
Overall, an Adobe XD prototype was created through this iterative process, in which new design properties and technical functions were tried out again and again, which represents the most important information for communities. The information is prepared in a particularly understandable way, for example with the progress display. However, for meaningful use, a consultant with detailed knowledge of current Digital Impact Labs and their equipment, staff and range is required. Furthermore, the current prototype is only for use on the desktop, which I consider to be central here.
I am convinced that our prototype can help to facilitate the creation of Digital Impact Labs. In addition to the easier creation, I hope that I will also be quality improvements due to improved planning and improved communication between different stakeholders due to the improved presentation of information.
The technical implementation is still pending for my prototypes. However, this can also be an advantage, since developers who want to build on our prototypes can think of their own technical path that is based on your skills as a developer. In addition, developers can better integrate their own ideas.

</p>



<img  src="/images/Toolbuilding2/toolbuilding-pro1.png" alt="Secreenshots of Digital Impact Lab" />
 <p style="font-size:150%">Login page for Consultant </p>
 
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro2.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Location selection by categories</p>
 
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro3.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Personal options</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro4.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Network options</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro5.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Tools</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro6.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Requirements to build a digital impact lab</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro7.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Mind map</p>

<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro8.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Suggestions</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro9.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Suggestions</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro10.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Suggestions</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro11.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Requirements to build a digital impact lab</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro12.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Mind map</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro13.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Suggestions</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro14.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Suggestions</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro15.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Suggestions</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro16.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Cost overview to calculate expenses</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro17.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Cost overview as a pie chart</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro18.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Contact form</p>
<br>
<img  src="/images/Toolbuilding2/toolbuilding-pro18.png" alt="Secreenshots of Digital Impact Lab" />
<p style="font-size:150%">Contact form</p>
<br>
<h3> Alternative prototyps </h3>
<img  src="/images/Toolbuilding/toolbuilding2-pro1.png" alt="Secreenshots of Digital Impact Lab" />
<br>
<img  src="/images/Toolbuilding/toolbuilding2-pro2.png" alt="Secreenshots of Digital Impact Lab" />
<br>

