---
title: "Java Eshop Application "

date: 2019-10-14T15:40:24+06:00
image : "images/Prog/eshop3.0.png"

# author
author : ["Diyar"]
# categories
categories: ["Applications"]
tags: ["Java Application", "Eshop", "Eclipse"]
# meta description
description: "This is meta description"
# save as draft
draft: false

---
<p style="font-size:150%; text-align: left;">
One of the first java projects I did in my second semester, it's not a project I'm proud of, but it was a good experience throughout the semester, it contributed to my better understanding of objective oriented programming.</p>
<br>
<p style="font-size:150%; text-align: left;">It is a multi-functional java application, in which stocks of items, different options and actions of users and employees can be tracked.
Inventory of items, shopping and user movements can be written to a text file on the computer, read from there to displayed in the Gui.
</p>
<div class>
<img  src="/images/Prog/eshop8.png" alt="Secreenshots of Eshop" style="text-align:left;"/>
<p style="font-size:120%">
Register or Login
</p>
<br>
<img  src="/images/Prog/eshop5.png" alt="Secreenshots of Eshop" />
<p style="font-size:120%">
Register a Customer, which also can be done from an Employee.
When a customer is created, the program goes through the array of customer numbers and gives the next free number.
</p>
<br>
<img  src="/images/Prog/eshop4.png" alt="Secreenshots of Eshop" />
<p style="font-size:120%">Storage field of employee menu.</p>
<br>
<img  src="/images/Prog/eshop3.png" alt="Secreenshots of Eshop" />
<p style="font-size:120%">Changelog menu where user movements can be tracked.</p>
<br>
<img  src="/images/Prog/eshop6.png" alt="Secreenshots of Eshop" />
<p style="font-size:120%">Shopping cart with selected items.</p>
<br>
<img  src="/images/Prog/eshop7.png" alt="Secreenshots of Eshop" />
<p style="font-size:120%">Bill is automatically generated after the transaction is completed.</p>
<br>