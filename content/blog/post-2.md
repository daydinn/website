---
title: "Prototyp of a Web Extension"
date: 2022-06-25T15:40:24+06:00
image: "images/DFPWSN/extension1.png"
# author
author : ["Admin"]
# categories
categories: ["Prototyps"]
tage: ["Prototyp","Web Extension","Adobe XD"]
# meta description
description: "This is meta description"
# save as draft
draft: false
---

<h3>Animated prototype of a web extension for people with color
                  blindness made with ADOBE XD </h3>

<h4>Introduction and Motivation</h4>
<p style="font-size:120%">About 5% of the German population suffers from color blindness. The big part of this group are men, around the
8% because color blindness is inherited via the X chromosome. Color blindness is often stereotyped in which
People believe that color blind people only see shades of gray. This occurs however only with a rare form of the Tritanopia
(Blue blindness) on, in Germany about 3000 people, in which also the light receptors, which are responsible for the color blue, the
eyes are affected. Most color blind people just lack red or green cones, which is what
a so-called red-green weakness (deuteranopia) leads to different green and more difficult people
Red tones can be recognized and the two colors can be used in low light conditions or fast-moving objects
confound.
Since I know one person with a red-green weakness, Max Schönemann, I had the idea
to make everyday life easier for people who are colour-blind or colour-blind. The solution is designed to help color blind or color blind people holistically.
</p>

<h4>Concept & Development</h4>
<p style="font-size:120%">
The idea behind my concept is to provide color blind people with an optimized browser
enable experience. The solution should be a browser extension that will help color blind people and color blind people
fully supported in the browser.
I developed a Adobe XD prototype. All other groups liked the general handling of the program. 
However, I failed to code this prototype as well, since our algorithm was not
led to the desired results
</p>
<br>
<br>


<img  src="/images/DFPWSN/extension1.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">This is how people who suffer from deuteranopia(green blindness) see the colors.</p>
<br>
<img  src="/images/DFPWSN/extension2.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">How the colors really look like.</p>
<br>
<img  src="/images/DFPWSN/extension3.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">Login & Register.</p>
<br>
<img  src="/images/DFPWSN/extension4.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">Login form.</p>

<br>
<img  src="/images/DFPWSN/extension5.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">Register form.</p>
<br>
<img  src="/images/DFPWSN/extension6.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">Colors before using the Web extension.</p>

<br>
<img  src="/images/DFPWSN/extension7.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">
Automatically adjusted colors after using the extension.</p>

<br>
<img  src="/images/DFPWSN/extension8.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">This is how people who suffer from Tritanopie(blue blindness) see the colors.</p>
<br>
<img  src="/images/DFPWSN/extension9.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">How the colors really look like.</p>

<br>
<img  src="/images/DFPWSN/extension10.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">
Automatically adjusted colors after using the extension.</p>
<br>
<img  src="/images/DFPWSN/extension11.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">This is how people who suffer from Protanopie(red blindness) see the colors.</p>
<br>

<img  src="/images/DFPWSN/extension13.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">
<p style="font-size: 150%">How the colors really look like.</p>
<br>
<img  src="/images/DFPWSN/extension14.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">
Automatically adjusted colors after using the extension.</p>
<br>
<img  src="/images/DFPWSN/extension15.png" alt="Secreenshot of Webextension for people with color blindness" />
<p style="font-size: 150%">
Settings.</p>
<br>


