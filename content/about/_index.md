---
title: "Diyar Aydin"
# meta description
description: "This is meta description"
# save as draft
draft: false
---
### <h3 style="text-align:center;">Front End Developer & UX/UI Designer</h3>
<div class="col-md-8">
<br>
<br>
<br>
<br>
<p class="subheading" style="font-size: 150%;" >
  Skills which I have acquired through my practice-oriented studies:
</p>
<h3 style="font-size: 150%;" class="progress-title">HTML5</h3>
<p style="font-size:110%">I've used this Markup Language in most of my projects and can use it competently.
  When using HTML my focus lies on creating aesthetically pleasing and responsive websites.</p>
<div class="progress">
  <div class="progress-bar" style="width: 95%; background: #ef2d56">
    <br>
  </div>
</div>

<h3 style="font-size: 150%;" class="progress-title">CSS</h3>
<p style="font-size:110%">I can find practical and effective solutions for improving the design of websites. I
  am able to use it effective for almost all situations.</p>

<div class="progress">
  <div class="progress-bar" style="width: 90%; background: #00ff00">
    <br>
  </div>
</div>
<h3 style="font-size: 150%;" class="progress-title">JS</h3>
<p style="font-size:110%">I use Javascript to make websites interactive. Allthough I have not yet mastered
  Javascript, I have used it many projects which I have worked on. I have completed many courses on Javascript
  on udemy. If you wan't to know which courses I have completed, please message me </p>
<div class="progress">
  <div class="progress-bar" style="width: 80%; background: #ff9900">
    <br>
  </div>
</div>

<h3 style="font-size: 150%;" class="progress-title">Java</h3>
<p style="font-size:110%">Is the first object oriented programming language, that I learned at university.
  I have gained practical experience by programming an ecommerce application for my University and read "Head
  First Java"
  in order to understand more of the language.
</p>

<div class="progress">
  <div class="progress-bar" style="width: 75%; background: #00ffff">
    <br>
  </div>
</div>
<h3 style="font-size: 150%;" class="progress-title">C++</h3>
<p style="font-size:110%">I used this language in my Open-GL projects for university in order to create
  animations for my course "Computergraphics.
</p>
<div class="progress">
  <div class="progress-bar" style="width: 45%; background: #cc66ff">
    <br>
  </div>
</div>
<h3 style="font-size: 150%;" class="progress-title">MySQL</h3>
<p style="font-size:110%">In the course "Databases for web applications" of the 4th semester,
  I used MySQL to save attributes in a database of a Webshop regarding the customers orders, their addreses
  and the products of the shop.
  Although my interests lies in frontend development, I an profficent in MySQL.
</p>
<div class="progress">
  <div class="progress-bar" style="width: 60%; background: #ffff66">
    <br>
  </div>
</div>
<h3 style="font-size: 150%;" class="progress-title">Angular</h3>
<p style="font-size:110%">In the 4th semester I developed a client-side online coffee shop application
  using MySQL and the Angular Framework.
</p>

<div class="progress">
  <div class="progress-bar" style="width: 70%; background: #3366ff">
  <br>
  </div>
</div>
<h3 style="font-size: 150%;" class="progress-title">Adobe Illustrator</h3>
<p style="font-size:110%">The application I regularly use with pleasure to design logos, icons, CV's or
  banners.</p>
<div class="progress">
  <div class="progress-bar" style="width: 90%; background: #6600cc">
    <br>
  </div>
</div>
<h3 style="font-size: 150%;" class="progress-title">Adobe XD</h3>
<p style="font-size:110%">Useful application for mockups, where I design the prototypes of web browser
  extencions, websites or apps.</p>
<div class="progress">

  <div class="progress-bar" style="width: 90%; background: #ffc266">
    <br>
  </div>
</div>
<h3 style="font-size: 150%;" class="progress-title">Adobe Indesign</h3>
<p style="font-size:110%">I've used Indesign a few times, it offers good layout support for digital media like
  magazines or posters.</p>
<div class="progress">

  <div class="progress-bar" style="width: 80%; background: #00ccff">
    <br>
  </div>
</div>
<h3 style="font-size: 150%;" class="progress-title">Photoshop</h3>
<p style="font-size:110%">I'm definetly not an expert but I have used it for the most necessary things like
  setting the color tone of pictures or cropping. Still one of the adobe applications that I enjoy and want to
  improve my skills.</p>
<div class="progress">

  <div class="progress-bar" style="width: 70%; background: #ff3399">
    <br>
  </div>
</div>
</div>
<div class="col-md-2"> </div>
      